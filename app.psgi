#!/usr/bin/perl
use v5.14;
use warnings;

use Gruntmaster::Data;
use Plack::App::Gruntmaster;
use Plack::Builder;
use Plack::Util;
use Log::Log4perl;

sub CONTENT_SECURITY_POLICY () {
	my $csp = <<CSP;
default-src 'none'
connect-src 'self'
form-action 'self'
frame-ancestors 'none'
img-src 'self'
referrer origin-when-cross-origin
script-src 'self'
style-src 'self'
CSP
	chomp $csp;
	$csp =~ s/\n/; /gr;
}

sub add_database {
	my $app = $_[0];
	sub {
		dbinit $ENV{GRUNTMASTER_DSN} // 'dbi:Pg:' unless db;
		$app->(@_)
	}
}

sub add_headers {
	my $app = $_[0];
	sub {
		my $resp = $app->($_[0]);
		my $hdrs = Plack::Util::headers($resp->[1]);
		$hdrs->set('Content-Security-Policy', CONTENT_SECURITY_POLICY);
		$hdrs->set('Cache-Control', 'public, max-age=604800') if $_[0]->{PATH_INFO} =~ qr,^/static/,;
		$resp->[1] = $hdrs->headers;
		$resp;
	}
}

Log::Log4perl->init_once('log.conf');
$ENV{DBIC_NULLABLE_KEY_NOWARN} = 1;

builder {
	enable_if { $_[0]->{PATH_INFO} eq '/ok' } sub { sub{ [200, [], []] }};
	enable 'ContentLength';
	enable \&add_headers;
	enable 'Static', path => qr,^/static/,;
	enable 'Log4perl', category => 'plack';
	enable \&add_database;
	enable '+Plack::App::Gruntmaster::Auth',
	  dbi_connect => [$ENV{GRUNTMASTER_DSN} // 'dbi:Pg:', '', ''],
	  realm       => 'Gruntmaster 6000',
	  mail_from   => $ENV{GRUNTMASTER_RESET_FROM};
	Plack::App::Gruntmaster->run_if_script
}
