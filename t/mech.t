#!/usr/bin/perl -w
use strict;
use warnings;

use Test::More;
use Test::WWW::Mechanize::PSGI;

use Gruntmaster::Data;
use JSON::MaybeXS qw/decode_json/;
use Log::Log4perl qw/:easy/;

BEGIN {
	eval {
		dbinit 'dbi:Pg:dbname=gmtest'; 1;
	} or plan skip_all => 'Cannot connect to test database (gmtest).'. "Error: $@";
	plan tests => 22;
}

Log::Log4perl->easy_init($OFF);
my $mech = Test::WWW::Mechanize::PSGI->new(app => do 'app.psgi');

$mech->get_ok('/');
$mech->title_is('Gruntmaster 6000', 'title');

$mech->get_ok('/pb/');
$mech->title_is('Problems', 'title');

$mech->get_ok('/pb/arc');
$mech->title_is('Problem in archive', 'title');

$mech->get_ok('/ct/');
$mech->title_is('Contests', 'title');

$mech->get_ok('/ct/fc');
$mech->title_is('Finished contest');

$mech->get_ok('/log/');
$mech->title_is('Job log', 'title');

$mech->get_ok('/log/1');
$mech->title_is('Job 1', 'title');

$mech->get_ok('/us/');
$mech->title_is('Users', 'title');

$mech->get_ok('/us/MGV');
# testdata.sql does not set name for users, therefore not checking title
#$mech->title_is('Marius Gavrilescu', 'title');

$mech->get_ok('/src/1.c');

$mech->get_ok('/st/fc');
$mech->title_is('Standings', 'title');

$mech->get_ok('/ed/fc');
$mech->title_is('Editorial of Finished contest', 'title');
