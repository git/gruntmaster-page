package Plack::App::Gruntmaster::HTML;
use v5.14;
use parent qw/Exporter/;
our @EXPORT = qw/render render_article/;

use File::Slurp qw/read_file/;
use List::Util qw[min max];
use HTML::Element::Library;
use HTML::TreeBuilder;
use POSIX qw//;
use Data::Dumper qw/Dumper/;
use Sort::ByExample
  sorter => {-as => 'ct_sort', example => [qw/Running Pending Finished/], xform => sub {$_->{status}}};

my $optional_end_tags = {%HTML::Tagset::optionalEndTag, tr => 1, td => 1, th => 1};

sub ftime ($)   { POSIX::strftime '%c', localtime shift }
sub literal ($) {
	my ($html) = @_;
	return '' unless $html;
	my $b = HTML::TreeBuilder->new;
	$b->ignore_unknown(0);
	$b->parse($html);
	HTML::Element::Library::super_literal $b->guts->as_HTML(undef, undef, $optional_end_tags);
}

sub HTML::Element::edit_href {
	my ($self, $sub) = @_;
	local $_ = $self->attr('href');
	$sub->();
	$self->attr(href => $_);
}

sub HTML::Element::iter3 {
	my ($self, $data, $code) = @_;
	my $orig = $self;
	my $prev = $orig;
	for my $el (@$data) {
		my $current = $orig->clone;
		$code->($el, $current);
		$prev->postinsert($current);
		$prev = $current;
	}
	$orig->detach;
}

sub HTML::Element::fid    { shift->look_down(id    => shift) }
sub HTML::Element::fclass { shift->look_down(class => qr/\b$_[0]\b/) }

sub HTML::Element::namedlink {
	my ($self, $id, $name) = @_;
	$name = $id unless $name && $name =~ /[[:graph:]]/;
	$self = $self->find('a');
	$self->edit_href(sub {s/id/$id/});
	$self->replace_content($name);
}

my %page_cache;
for (<tmpl/*>) {
	my ($tmpl, $lang) = m,tmpl/(\w+)\.(\w+),;
	my $builder = HTML::TreeBuilder->new;
	$builder->ignore_unknown(0);
	$page_cache{$tmpl, $lang} = $builder->parse_file($_);
}

sub render {
	my ($tmpl, $lang, %args) = @_;
	$lang //= 'en';
	my $meat = _render($tmpl, $lang, %args);
	my $html = _render('skel', $lang, %args, meat => $meat);
	if ($tmpl eq 'pb_entry') { # Move sidebar to correct position
		my $builder = HTML::TreeBuilder->new;
		$builder->ignore_unknown(0);
		my $tree = $builder->parse_content($html);
		$tree->fid('content')->postinsert($tree->fid('sidebar'));
		$html = $tree->as_HTML(undef, undef, $optional_end_tags)
	}
	$html
}

sub render_article {
	my ($art, $lang, %args) = @_;
	$lang //= 'en';
	my $title = read_file "a/$art.$lang.title";
	chomp $title;
	my $meat  = read_file "a/$art.$lang";
	_render('skel', $lang, title => $title , meat => $meat, %args)
}

sub _render {
	my ($tmpl, $lang, %args) = @_;
	my $tree = $page_cache{$tmpl, $lang}->clone or die "No such template/language combination: $tmpl/$lang\n";
	$tree = $tree->guts unless $tmpl eq 'skel';
	$tree->defmap(smap => \%args);
	my $process = __PACKAGE__->can("process_$tmpl");
	$process->($tree, %args) if $process;
	$_->attr('smap', undef) for $tree->look_down(sub {$_[0]->attr('smap')});
	$tree->as_HTML(undef, undef, $optional_end_tags);
}

sub process_skel {
	my ($tree, %args) = @_;
	$tree->content_handler(
		title   =>         $args{title},
		content => literal $args{meat});
}

sub process_us_entry {
	my ($tree, %args) = @_;
	$tree->fid($_)->attr('href', "/$_/?owner=$args{id}") for qw/log pb/;
	$tree->fid('track_user')->attr('data-user', $args{id});
	my @solved = map { $_->{solved} ? ($_->{problem}) : () } @{$args{problems}};
	my @attempted = map { !$_->{solved} ? ($_->{problem}) : () } @{$args{problems}};

	my $pbiter = sub {
		my ($data, $li) = @_;
		$li->find('a')->namedlink($data);
	};
	$tree->fid('solved')->find('li')->iter3(\@solved, $pbiter);
	$tree->fid('attempted')->find('li')->iter3(\@attempted, $pbiter);
	$tree->fid('solved_count')->replace_content(scalar @solved);
	$tree->fid('attempted_count')->replace_content(scalar @attempted);

	my $ctiter = sub {
		my ($data, $td) = @_;
		$td->fclass('contest')->namedlink($data->{contest}, $data->{contest_name});
		$td->fclass('score')->replace_content($data->{score});
		$td->fclass('rank')->replace_content($data->{rank});
	};
	$tree->find('table')->find('tbody')->find('tr')->iter3($args{contests}, $ctiter);
}

sub process_us {
	my ($tree, %args) = @_;
	my $iter = sub {
		my ($data, $tr) = @_;
		$tr->fclass('user')->namedlink($data->{id}, $data->{name});
		$tr->fclass($_)->replace_content($data->{$_}) for qw/solved attempted contests/;
	};
	$tree->find('tbody')->find('tr')->iter3($args{us}, $iter);
}

sub process_ct_entry {
	my ($tree, %args) = @_;
	$_->edit_href (sub {s/contest_id/$args{id}/}) for $tree->find('a');
	$tree->fid('editorial')->detach unless $args{finished};
	$tree->fid('links')->detach unless $args{started};
	my $status = ($args{time} < $args{start} ? 'starts' : 'ends');
	$tree->fclass('timer')->attr('data-stop', $status eq 'ends' ? $args{stop} : $args{start});
	$tree->content_handler(
		start       => ftime   $args{start},
		stop        => ftime   $args{stop},
		status      => $status,
		description => literal $args{description});
	$tree->fid('ctcountdown')->detach if $args{time} >= $args{stop};
}

sub process_ct {
	my ($tree, %args) = @_;
	my $iter = sub {
		my ($data, $tr) = @_;
		$data->{$_} = ftime $data->{$_} for qw/start stop/;
		$tr->hashmap(class => $data, [qw/name owner/]);
		$tr->fclass('name')->namedlink($data->{id}, $data->{name});
		$tr->fclass('owner')->namedlink($data->{owner}, $data->{owner_name});
	};
	$_->{status} = $_->{finished} ? 'Finished' : $_->{started} ? 'Running' : 'Pending' for @{$args{ct}};
	$tree->find('tbody')->find('tr')->iter3([ct_sort @{$args{ct}}], $iter);
}

sub process_pb_entry {
	my ($tree, %args) = @_;
	$tree->fid('owner')->edit_href(sub{s/owner_id/$args{owner}/});
	$tree->fid('job_log')->edit_href(sub{s/problem_id/$args{id}/});
	$tree->fid('solution')->edit_href(sub{s/problem_id/$args{id}/});
	$tree->content_handler(
		statement => literal $args{statement},
		level     => ucfirst $args{level},
		author    =>         $args{author},
		owner     =>         $args{owner_name} || $args{owner});
	if ($args{limits}) {
		my @limits = (@{$args{limits}}, {format => 'Other', timeout => $args{timeout} });
		@limits = map { sprintf '%s (%s)', @{$_}{qw/timeout format/} } @limits;
		$tree->look_down(smap => 'timeout')->replace_content(join ', ', @limits);
	}
	if ($args{contest_stop}) {
		$tree->fid('solution')->detach;
		$tree->fid('solution_modal')->detach;
		my $score = $tree->fid('score');
		$score->attr('data-start' => $args{open_time});
		$score->attr('data-stop'  => $args{contest_stop});
		$score->attr('data-value'  => $args{value});
		$tree->fid('countdown')->attr('data-stop' => $args{contest_stop});
	} else {
		$tree->fid('job_log')->edit_href(sub{$_ .= "&private=$args{private}"}) if $args{private};
		$tree->fid('solution')->detach unless $args{solution};
		$_->detach for $tree->fclass('rc'); # requires contest
		$tree->fid('solution_modal')->replace_content(literal $args{solution});
	}

	$tree->look_down(name => 'problem')->attr(value => $args{id});
	my $contest = $tree->look_down(name => 'contest');
	$contest->attr(value => $args{args}{contest}) if $args{args}{contest};
	$contest->detach unless $args{args}{contest}
}

sub process_sol {
	my ($tree, %args) = @_;
	$tree->content_handler(solution => literal $args{solution});
}

sub process_pb {
	my ($tree, %args) = @_;
	my $iter = sub {
		my ($data, $tr) = @_;
		$tr->set_child_content(class => 'author', $data->{author});
		$tr->set_child_content(class => 'level', ucfirst $data->{level});
		$tr->fclass('name')->namedlink($data->{id}, $data->{name});
		$tr->fclass('name')->find('a')->edit_href(sub {$_ .= "?contest=$args{args}{contest}"}) if $args{args}{contest};
		$tr->fclass('owner')->namedlink($data->{owner}, $data->{owner_name});
		$tr->find('td')->attr(class => $tr->find('td')->attr('class').' warning') if $data->{private} && !$args{args}{contest};
	};

	$tree->find('tbody')->find('tr')->iter3([sort { $a->{value} <=> $b->{value} } @{$args{pb}}], $iter);
	$tree->fid('open-alert')->detach unless $args{args}{contest};
}

sub process_log_entry {
	my ($tree, %args) = @_;
	$tree->fid('problem')->namedlink(@args{qw/problem problem_name/});
	$tree->fid('owner')->namedlink(@args{qw/owner owner_name/});
	$tree->fid('source')->namedlink("$args{id}.$args{extension}", sprintf '%.2fKB', $args{size}/1024);
	if ($args{contest}) {
		$tree->fid('contest')->namedlink(@args{qw/contest contest_name/});
		$tree->fid('problem')->find('a')->edit_href(sub {$_.="?contest=$args{contest}"});
	} else {
		$tree->fid('contest')->left->detach;
		$tree->fid('contest')->detach;
	}

	$args{errors} ? $tree->fid('errors')->find('pre')->replace_content($args{errors}) : $tree->fid('errors')->detach;
	my $iter = sub {
		my ($data, $tr) = @_;
		$data->{time} = sprintf '%.4fs', $data->{time};
		$tr->defmap(class => $data);
		$tr->fclass('result_text')->attr(class => "r$data->{result}")
	};
	$tree->fclass('result_text')->replace_content($args{result_text});
	$tree->fclass('result_text')->attr(class => "r$args{result}");
	$args{results} ? $tree->fid('results')->find('tbody')->find('tr')->iter3($args{results}, $iter) : $tree->fid('results')->detach;
	$tree->fid('no_results')->detach if $tree->fid('results') || $tree->fid('errors');
}

sub process_log {
	my ($tree, %args) = @_;
	my $iter = sub {
		my ($data, $tr) = @_;
		$tr->fclass('id')->namedlink($data->{id});
		$tr->fclass('problem')->namedlink($data->{problem}, $data->{problem_name});
		$tr->fclass('problem')->find('a')->edit_href(sub{$_ .= '?contest='.$data->{contest}}) if $data->{contest};
		$tr->fclass('contest')->namedlink($data->{contest}, $data->{contest_name}) if $data->{contest};
		$tr->fclass('contest')->replace_content('None') unless $data->{contest};
		$tr->fclass('date')->replace_content(ftime $data->{date});
		$tr->fclass('source')->namedlink("$data->{id}.$data->{extension}", sprintf "%.2fKB %s", $data->{size}/1024, Plack::App::Gruntmaster::FORMAT_EXTENSION()->{$data->{format}});
		$tr->fclass('owner')->namedlink($data->{owner}, $data->{owner_name});
		$tr->fclass('result_text')->replace_content($data->{result_text});
		$tr->fclass('result_text')->attr(class => "r$data->{result}");
		$tr->find('td')->attr(class => $tr->find('td')->attr('class').' warning') if $data->{private};
	};
	$tree->find('table')->find('tbody')->find('tr')->iter3($args{log}, $iter);
	$args{next_page} ? $tree->fclass('next')->namedlink($args{next_page}, 'Next') : $tree->fclass('next')->detach;
	$args{previous_page} ? $tree->fclass('previous')->namedlink($args{previous_page}, 'Previous') : $tree->fclass('previous')->detach;
	for my $cls (qw/next previous/) {
		my $elem = $tree->fclass($cls);
		next unless $elem;
		delete $args{args}{page};
		my $str = join '&', map { $_ . '=' . $args{args}{$_} } keys %{$args{args}};
		$elem->find('a')->edit_href(sub{s/$/&$str/}) if $str;
	}

	my $total_pages = max(1, $args{last_page});
	$tree->fclass('current')->replace_content("Page $args{current_page} of $total_pages");
}

sub process_st {
	my ($tree, %args) = @_;
	$args{problems} //= [];
	my $pbiter = sub {
		my ($data, $th) = @_;
		$th->attr(class => undef);
		$th->namedlink(@$data);
		$th->find('a')->edit_href(sub{s/$/?contest=$args{args}{contest}/});
	};
	$tree->fclass('problem')->iter3($args{problems}, $pbiter);
	my $iter = sub {
		my ($st, $tr) = @_;
		$tr->set_child_content(class => 'rank', $st->{rank});
		$tr->set_child_content(class => 'score', $st->{score});
		$tr->fclass('user')->namedlink($st->{user}, $st->{user_name});
		my $pbscore = $tr->fclass('pbscore');
		$pbscore->iter($pbscore => @{$st->{scores}});
	};
	$tree->find('tbody')->find('tr')->iter3($args{st}, $iter);
}

sub process_ed {
	my ($tree, %args) = @_;
	$tree->content_handler(editorial => literal $args{editorial});
	my $iter = sub {
		my ($data, $div) = @_;
		$div->set_child_content(class => 'value', $data->{value});
		$div->set_child_content(class => 'solution', literal $data->{solution});
		$div->fclass('problem')->namedlink($data->{id}, $data->{name});
	};
	my @pb = sort { $a->{value} <=> $b->{value} } @{$args{pb}};
	$tree->fclass('well')->iter3(\@pb, $iter);
}

1;
__END__
