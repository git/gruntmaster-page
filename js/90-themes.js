function set_style(name, trans){
	if(trans){
		document.body.classList.add('transition-color');
		setTimeout(() => document.body.classList.remove('transition-color'), 1000);
	}
	if(q('html').className == name)
		return;
	q('html').className = name;
	localStorage.setItem("theme", name);
}

$(function() {
	const theme_ul = '<ul class="list-inline" id="themes">' +
		'<li><a role="button" data-theme="cyborg">Black</a>' +
		'<li><a role="button" data-theme="slate">Grey</a>' +
		'<li><a role="button" data-theme="readable">White</a></ul>';
	const sidebar = q('#sidebar');
	sidebar.insertBefore(m(theme_ul), sidebar.firstChild);
	$('#themes a').on('click', e => set_style(e.target.dataset.theme, true));
});

const theme = localStorage.getItem("theme");
if(theme)
	set_style(theme == "cerulean" || theme == "cosmo" ? "readable" : theme, false);
