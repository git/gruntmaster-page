const TICK  = '<span class="tick tracker-mark">✔</span>';
const XMARK = '<span class="x tracker-mark">✘</span>';

function clean_tracker(){
	$('.tracker-mark').each(e => e.parentNode.removeChild(e));
	$('.tracker-info').each(e => {
		e.classList.remove('tracker-info');
		e.classList.remove('info');
	});
}

let tracker;

function update_tracker(){
	if(!localStorage.getItem('tracker_data'))
		return;
	const data = JSON.parse(localStorage.getItem('tracker_data'));
	const user = localStorage.getItem('tracker_username');
	const contests = {}, solved = {}, attempted = {};
	let solvednr = 0, attemptednr = 0;
	data.problems.forEach(e => {
		if(e.solved) {
			solved[e.problem] = 1;
			solvednr++;
		} else {
			attempted[e.problem] = 1;
			attemptednr++;
		}
	});

	data.contests.forEach(e => contests[e.contest] = 1);

	clean_tracker();
	q('#tracker_userlink').innerHTML = data.name;
	q('#tracker_solved').innerHTML = solvednr;
	q('#tracker_attempted').innerHTML = attemptednr;
	q('#tracker_contests').innerHTML = data.contests.length;
	q('#tracker_log').setAttribute('href', '/log/?owner=' + user);

	if(location.pathname == '/pb/')
		$('table .name a').each(el => {
			const id = el.getAttribute('href').split('?', 2)[0];
			if(solved[id])
				el.insertAdjacentHTML('beforebegin', TICK);
			else if(attempted[id])
				el.insertAdjacentHTML('beforebegin', XMARK);
		});

	if(location.pathname == '/ct/')
		$('table .name a').each(el => {
			const id = el.getAttribute('href').substr(4);
			if(contests[id])
				el.insertAdjacentHTML('beforebegin', TICK);
		});

	if(location.pathname == '/log/')
		$('table tbody tr').each(el => {
			const owner = el.querySelector('.owner a');
			if(!owner || owner.getAttribute('href') != '/us/' + user)
				return;
			const state = el.getElementsByClassName('r0').length ? TICK : XMARK;
			el.getElementsByClassName('id')[0].innerHTML += state;
		});

	if(location.pathname.match(/^\/st\//) || location.pathname == '/us/')
		$('table tbody tr').each(el => {
			if(el.querySelector('.user a').getAttribute('href') == '/us/' + user){
				el.classList.add('info');
				el.classList.add('tracker-info');
			}
		});
}

function start_tracking(user){
	localStorage.setItem('tracker_username', user);
	q('#tracker_userlink').setAttribute('href', '/us/' + user);
	q('#tracker_userlink').innerHTML = user;
	tracker.classList.remove('hidden');
	update_tracker();
	const lastfetch = localStorage.getItem('tracker_lastfetch');
	if(Date.now() - lastfetch > 60 * 10 * 1000)
		refresh_tracker();
}

function refresh_tracker(){
	const user = localStorage.getItem('tracker_username');
	const xhr = new XMLHttpRequest();
	xhr.open('GET', '/us/' + user + '?format=json');
	xhr.onload = () => {
		localStorage.setItem('tracker_data', xhr.responseText);
		localStorage.setItem('tracker_lastfetch', Date.now());
		update_tracker();
	};
	xhr.send();
}

function stop_tracking(){
	clean_tracker();
	localStorage.removeItem('tracker_username');
	localStorage.removeItem('tracker_data');
	localStorage.removeItem('tracker_lastfetch');
	tracker.classList.add('hidden');
}

$(function(){
	tracker = m('<div id="tracker" class="hidden">Tracking <a id="tracker_userlink"></a>.<br><a id="tracker_stop" role="button">Stop tracking</a><br><a id="tracker_log">Job log</a><dl class="dl-horizontal"><dt>Solved</dt>    <dd id="tracker_solved">?</dd><dt>Attempted</dt> <dd id="tracker_attempted">?</dd><dt>Contests</dt>  <dd id="tracker_contests">?</dd></dl></div>');
	const sidebar = q('#sidebar');
	sidebar.insertBefore(tracker, sidebar.firstChild);
	$('#tracker_stop').on('click', stop_tracking);
	$('#track_user').on('click', function() { stop_tracking(); start_tracking(this.dataset.user) });
	$('#submitform').on('submit', () => localStorage.removeItem('tracker_lastfetch'));

	if(localStorage.getItem('tracker_username'))
		start_tracking(localStorage.getItem('tracker_username'));
});
