const ext_table = {
	'bf'  : 'BRAINFUCK',

	'c'   : 'C',

	'd'   : 'D',

	'cc'  : 'CPP',
	'cpp' : 'CPP',
	'cxx' : 'CPP',
	'C'   : 'CPP',

	'go'  : 'GOLANG',

	'gs'  : 'GOLFSCRIPT',

	'hs'  : 'HASKELL',
	'lhs' : 'HASKELL',

	'lisp': 'SBCL',
	'lsp' : 'SBCL',
	'cl'  : 'SBCL',
	'l'   : 'SBCL',

	'java': 'JAVA',

	'jl'  : 'JULIA',

	'ml'  : 'OCAML',

	'pas' : 'PASCAL',

	'pl'  : 'PERL',

	'php' : 'PHP',

	'py'  : 'PYTHON',

	'rb'  : 'RUBY',

	'rs'  : 'RUST'
}

function divine_format() {
	const filename = q('#prog').value;
	const ext = /\.([^.]*)$/.exec(filename)[1];

	if(ext_table[ext])
		q('#prog_format').value = ext_table[ext];
}

$(() => $('#prog').on('change', divine_format));
