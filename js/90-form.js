$(function(){
	const result = q('#result');
	$('.jsform > input[type="submit"]').on('click', e => {
		const form_data = new FormData(this.parentElement);
		const form = this.parentNode;
		const xhr = new XMLHttpRequest();
		xhr.open(form.getAttribute('method'), form.getAttribute('action'));
		xhr.onload = () => result.innerHTML = this.responseText;
		xhr.onerror = () => result.innerHTML = 'Error!';
		window.scrollTo(0, 0);
		result.innerHTML = 'Loading...';
		xhr.send(form_data);
		e.preventDefault();
	});
});
