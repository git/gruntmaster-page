const NAVS = {
	home: /^\/$/,
	pb: /^\/(?:pb|sol)\//,
	ct: /^\/(?:ct|st|ed)\//,
	log: /^\/log\//,
	us: /^\/us\//,
	account: /^\/account$/,
	contribute: /^\/contribute$/,
};

$(function(){
	const path = location.pathname;
	for (const nav in NAVS)
		if(path.match(NAVS[nav]))
			q('#nav-' + nav).classList.add('active');
});
