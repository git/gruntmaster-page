$(function(){
	const sidebar = q('#sidebar');
	const login = document.createElement('div');
	login.innerHTML = '<a role="button" id="login" class="show">Log in</a>';
	sidebar.insertBefore(login, sidebar.firstChild);
	$('#login').on('click', e => {
		const xhr = new XMLHttpRequest();
		xhr.open('GET', '/login');
		xhr.onload = () => {
			if(xhr.status != 200)
				return;
			const username = xhr.responseText;
			sessionStorage.setItem('login_username', username);
			start_tracking(username);
			login.innerHTML = 'Probably logged in as ' + username;
		}
		xhr.send();
		return false;
	});

	const username = sessionStorage.getItem('login_username');
	if(username)
		login.innerHTML = 'Probably logged in as ' + username;
});
